Rails.application.routes.draw do
  # ここに、コントローラの後ろが追加されます。
  get 'sample/top'
  get 'sample/add_dummy_news'
  # get 'sample/news_detail'
  get 'sample/detail'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
