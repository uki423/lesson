class NewsFeed < ApplicationRecord
  #DBから全ニュースを取得します。
  def select
    return NewsFeed.all
  end
  #DBにニュースを追加します。
  def insert(url,news_type,timestamp)
    return NewsFeed.create(
      url:url,
      news_type:news_type,
      timestamp:timestamp,
    )
  end
  def select_target_news(id)
    return (NewsFeed.where(id:id))[0]
  end
end