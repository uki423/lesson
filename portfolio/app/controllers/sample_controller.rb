class SampleController < ApplicationController
  @news_lists
  @target_news
  # トップです
  def top
    news = NewsFeed.new
    @news_lists = news.select
    unless params[:news_type].nil?
      @news_lists = @news_lists.select{|r|
                    r["news_type"]==params[:news_type]}
    end
  end
  # ダミーニュース追加です
  def add_dummy_news
    news = NewsFeed.new
    time = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    news.insert("www.hoo.com","bussiness",time)
    redirect_to action: 'top'
  end
  # ニュース情報の詳細です
  def detail
    news = NewsFeed.new
    @target_news=news.select_target_news (params[:id])
    @target_news["encoded"]=CGI.unescapeHTML(@target_news["encoded"])
  end

end