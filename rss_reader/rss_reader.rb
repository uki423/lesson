#!ruby -Ku
require 'xmlsimple'
require 'date'
require 'json'
require 'cgi'
url=ARGV[0]
res = XmlSimple.xml_in(`curl -s "#{url}" -m 3`) rescue {}
ret = res["channel"][0]["item"].map{|news|
  {
      "title" => news["title"][0],
      "link" => news["link"][0],
      "timestamp" => (DateTime.parse(news["pubDate"][0])
                        .strftime("%Y-%m-%d %H:%M:%S") rescue ""),
      "description" => CGI.escapeHTML(news["description"][0]),
      "encoded" => CGI.escapeHTML(news["encoded"][0]),
      "image" => (news["encoded"][0].match(/(<img.+?src="(.+?)".+?>)/m)[2] rescue ""),
  }
} rescue []
puts JSON.generate(ret)