require 'xmlsimple'
require 'date'
require 'json'
require "csv"
def load_csv(csv=nil)
  res = []
  return res unless File.exists?(csv)
  csv_data = CSV.read(csv) rescue []
  columns =  csv_data.shift
  # カラムバリデーション
  if columns == nil or
      !(columns.instance_of?(Array) and columns.length > 0)
    return res
  end
  csv_data.each_with_index do |row,index|
    if row.length > 0
      res.push(Hash[*[columns,row].transpose.flatten])
    end
  end
  return res
end

RUBY = "/root/.rbenv/shims/ruby"
# RUBY = `/usr/bin/which ruby`
DIR = File.expand_path(File.dirname(__FILE__))
rss_list = load_csv("#{DIR}/rss_list.csv")

res = []
rss_list.each {|rss_row|
  datas = (JSON.parse(`#{RUBY} #{DIR}/rss_reader.rb #{rss_row["url"]}`) rescue [])
  datas.each{|dara_row|
    res.push(dara_row.merge({
     "id" => rss_row["id"],
     "type" => rss_row["type"],
    }))
  }
}
puts JSON.pretty_generate(res)