# １日ごとの収入/支出の結果を、日付ごとにソートして表示するプログラムを作る。

require 'pp'
require_relative './sql'

my=MysqlWrapper::new("localhost","root","","sato_lesson3")
columns = my.get_columns("sato_lesson3","cash_register_data")

select_res,err = my.query(q:<<STR,database: "sato_lesson3",columns: columns)
select * from cash_register_data order by date_and_time ASC;
STR
date_time_array = select_res.map {|r|r["date_and_time"]}.sort.uniq
res = date_time_array.map {|datetime|
  next {
      "datetime" => datetime,
      "summary" => select_res.select{|r|r["date_and_time"]==datetime}
                              .map{|r|r["in_or_out"]=="in"?
                                   r["amount_of_money"].to_i:
                                    (r["amount_of_money"].to_i*-1)}
                              .inject(:+),
  }
}
pp res
exit


