# 「種別」typeをコマンドラインパラメータで指定すると
# 当該のレコード一覧を表示するプログラムを作る。
require 'pp'
require_relative './sql'
my=MysqlWrapper::new("localhost","root","","sato_lesson3")
columns = my.get_columns("sato_lesson3","cash_register_data")

select_res,err = my.query(q:<<STR,database: "sato_lesson3",columns: columns)
select * from cash_register_data where type="#{ARGV[0]}";
STR
pp select_res
exit