# １．コマンドラインパラメータから、収入/支出(in/out),金額,使用用途,種別を指定して、履歴を登録するプログラムを作る。
require 'pp'
require_relative './sql'
my=MysqlWrapper::new("localhost","root","","sato_lesson3")
# カラム取得処理です。みたまんま、DBとテーブルを取得してください。
columns = my.get_columns("sato_lesson3","cash_register_data")

insert_res,err = my.query(q:<<STR,database: "sato_lesson3",columns: columns)
insert into cash_register_data set
  in_or_out = "#{ARGV[0]}",
  amount_of_money = "#{ARGV[1]}",
  use_applications = "#{ARGV[2]}",
  type = "#{ARGV[3]}",
  date_and_time = "#{ARGV[4]}"
STR

pp insert_res
exit
