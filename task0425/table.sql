CREATE TABLE news_feeds(
    id bigint NOT NULL auto_increment comment "ID",
    news_type varchar(255) default null comment "種別",
    image text default null comment "image url",
    title text default null comment "title",
    description text default null comment "短縮本文",
    encoded text default null comment "本文",
    link text default null comment "link",
    timestamp datetime default null comment "日時",
    PRIMARY KEY(id)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;