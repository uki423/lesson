# ２．収入/支出の合計額から、現在までの履歴を合算し
# 残額を表示するプログラムを作る
# "in"(収入)が"out"(支出)より多い場合は合算結果がプラスとなり、
# 逆に支出が多い場合はマイナスとなる
require 'pp'
require_relative './sql'
my=MysqlWrapper::new("localhost","root","","sato_lesson3")
columns = my.get_columns("sato_lesson3","cash_register_data")

select_res,err = my.query(q:<<STR,database: "sato_lesson3",columns: columns)
select SUM(amount_of_money) from cash_register_data;
STR
pp select_res
exit

#候補2パターン目
# select total=SUM(amount_of_money) from cash_register_data;
