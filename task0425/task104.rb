# １日ごとの収入/支出の結果を、日付ごとにソートして表示するプログラムを作る。

require 'pp'
require_relative './sql'

my=MysqlWrapper::new("localhost","root","","sato_lesson3")
columns = my.get_columns("sato_lesson3","cash_register_data")

select_res,err = my.query(q:<<STR,database: "sato_lesson3",columns: columns)
select date_format(`date_and_time`,'%Y%m%d')AS datetime,sum(`amount_of_money`)
AS summary from `cash_register_data` group by date_format(date_and_time,'%Y%m%d');
STR
pp select_res
exit