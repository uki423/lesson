# 3.第一引数で入力したWEBページのurlをから、WEBページの結果を取得し
# 　htmlページ内のすべてのimgタグの中身(http://xxxxxx.jpg/http://xxxxxx.png)
# を切り出すプログラムを作ってください。
# ※なお、WEBページの結果を取得するには「`curl -s URL名`」で取得可能です。
# バッククォートは、前に言ったコマンドの実行を意味します。

require 'pp'
# url = ARGV[0]
url = `curl -s  https://teams.one/teams/honmafriends`
# url = ARGV[0]
foo = url
res = foo.split(".")

#
# aaa = res.select! do |r|
#   r.include?("img")
# end

aaa = res.map do |f|
  f.match(/src=\"(.+?)\"/i)[1]
end

p aaa
