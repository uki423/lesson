#ユーザを新規に追加するプログラム
require 'pp'
require_relative './sql'
# mysqlお手製クラスを作ってみました。
# 4番目の引数にはテーブルを指定してください。
my=MysqlWrapper::new("localhost","root","","sato_lesson2")
# カラム取得処理です。みたまんま、DBとテーブルを取得してください。
columns = my.get_columns("sato_lesson2","user_data")





#selectの処理の見本です。
#
#
# この関数にはコロン付きの引数を使っています。
# 変数名の前に[引数名:]をつけます。
# これを使うと、パラメータが順不同で設定できるようになります。
# あと、デフォルト値も設定できます。
#
# resには結果、errにはエラーがはいります。(何も起きなければnilです)
select_res,err = my.query(q:<<STR,database: "sato_lesson2",columns: columns)
select * from yuki_store;
STR
pp select_res
exit

# insertはレスポンスを何も返しません。
insert_res,err = my.query(q:<<STR,database: "sato_lesson",columns: columns)
insert into user_data set
  user_name = "Danny Lee",
  age = "45",
  location = "North America"
STR

pp res
exit