class MysqlWrapper
  @my
  @@query_debug = false
  def initialize(host,user,password,database)
    if `mysql -h #{host} -u #{user} #{password} -N -B -e  "
      show databases like \\"%#{database}%\\""|wc -l`.to_i
      `mysql -h #{host} -u #{user} #{password} -N -B -e  "
      create database #{database}" > /dev/null 2>&1`
    end
  end

  def get_columns(database,table)
    columns_query="select column_name from  information_schema.columns where table_schema=\"#{database}\" and table_name=\"#{table}\""
    return `mysql -N -B --database #{database} -e '#{columns_query}' `.split(/\n/) rescue []
  end

  def get_connection
    return @my
  end

  # クエリ実行
  def query(q:"",columns:[],is_res_parse:true,database:"")
    begin
      db=database
      # ``でクエリ実行
      error = ""
      res = `mysql -N -B #{db} -e '#{q}' 2>&1`
      matches = res.match("^ERROR.*")
      error = matches[0] unless matches.nil?

      raise error if error.length > 0
      values = res.split(/\n/)
      ret = values.map{|row|
        row_hash = {}
        row.split(/\t/).each_with_index{|v,i|
          if(is_res_parse)
            # v.gsub!("\\0","\0")
            # v.gsub!("\\b","\b")
            v.gsub!("\\t","\t")
            v.gsub!("\\n","\n")
            v.gsub!("\\r","\r")
            # v.gsub!("\\x1a","\x1a")
          end
          # # v = v.gsub(/\\([a-z]{1,})/){next "\\#{$1}"}
          row_hash["#{columns[i]}"] = (v=="NULL")?
                                          nil:
                                          v.to_s.force_encoding('UTF-8')
        }
        next row_hash
      }
    rescue => e
      return [],e
    end
    return ret,nil
  end
end