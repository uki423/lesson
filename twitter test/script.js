function add_thread() {
    var tweet_text = document.getElementById("tweet-form").value


    if (tweet_text.length > 0) {
        var row_template = document.getElementsByClassName("time_line_row_template")[0]
        var row = row_template.cloneNode(true)
        row.style.display = "block"
        row.getElementsByClassName("message")[0].innerHTML = tweet_text
        document.getElementsByClassName("time_line")[0].appendChild(row)
    }
}